#include "IR_NRF52832_GPIO.h"

void IR_GPIOE_Callback(IR_NRF_GPIO_Interrupt_StreamTypeDef stream)
{
  if(stream == IR_NRF_GPIO_INTERRUPT_CHANNEL0)
  {
    IR_NRF_GPIO_Pin_Write(4, IR_NRF_GPIO_OUTPUT_SET);
  }
  if(stream == IR_NRF_GPIO_INTERRUPT_CHANNEL1)
  {
    IR_NRF_GPIO_Pin_Write(5, IR_NRF_GPIO_OUTPUT_SET);
  }
  if(stream == IR_NRF_GPIO_INTERRUPT_CHANNEL2)
  {
    IR_NRF_GPIO_Pin_Write(6, IR_NRF_GPIO_OUTPUT_SET);
  }
  if(stream == IR_NRF_GPIO_INTERRUPT_CHANNEL3)
  {
    IR_NRF_GPIO_Pin_Write(7, IR_NRF_GPIO_OUTPUT_SET);
  }
}

int main()
{
  IR_NRF_GPIO_Init(false);
  for(uint8_t  i = 4; i < 8; i++)
  {
    IR_NRF_GPIO_Output_Pin_Init(i, IR_NRF_GPIO_OUTPUT_RESET, IR_NRF_GPIO_DRIVE_STANDARD_0_STANDARD_1);
  }
  for (uint8_t i = 0; i < 4; i++)
  {
    IR_NRF_GPIO_EXTI_Pin_Init(i, IR_NRF_GPIO_MODE_EVENT, IR_NRF_GPIO_TRANSITION_TOGGLE, IR_NRF_GPIO_OUTPUT_RESET, i);
  }
  while (1)
  {
    //nothing for now
  }
}
  