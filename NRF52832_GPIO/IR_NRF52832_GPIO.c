#include "IR_NRF52832_GPIO.h"
//TODO: task mode should be written after PPI
//if(irq_en == true) then: irq_priority must be between 0 to 7
void IR_NRF_GPIO_Init(bool latch_en, bool irq_en, uint32_t irq_priority)
{
	NRF_P0->DETECTMODE = latch_en & 0x01;
	if(irq_en && irq_priority < 8)
  {
    NVIC_SetPriority(GPIOTE_IRQn, irq_priority);
    NVIC_EnableIRQ(GPIOTE_IRQn);
  }
}
void IR_NRF_GPIO_Deinit(void)
{
	NVIC_DisableIRQ(GPIOTE_IRQn);
	//TODO: clear everything
}
void IR_NRF_GPIO_Input_Pin_Init(uint8_t pin, IR_NRF_GPIO_PullTypeDef pull, IR_NRF_GPIO_SenseTypeDef sense)
{
  NRF_P0->PIN_CNF[pin] = (sense << 16) | (pull << 2);
}

void IR_NRF_GPIO_Output_Pin_Init(uint8_t pin, IR_NRF_GPIO_DriveTypeDef drive)
{
  NRF_P0->PIN_CNF[pin] = (drive << 8) | (0x01 << 1) | 0x01;
}

void IR_NRF_GPIO_Write_Pin(uint8_t pin, IR_NRF_GPIO_OutputTypeDef output)
{
  if(output == IR_NRF_GPIO_OUTPUT_SET)
	{
    NRF_P0->OUTSET = 1 << pin;
	}
  else if(output == IR_NRF_GPIO_OUTPUT_RESET)
	{
    NRF_P0->OUTCLR = 1 << pin;
	}
	else
	{
		if(NRF_P0->OUT & (1 << pin))
		{
			NRF_P0->OUTCLR = 1 << pin;
		}
		else
		{
			NRF_P0->OUTSET = 1 << pin;
		}
	}
}

bool IR_NRF_GPIO_Read_Pin(uint8_t pin)
{
  return (bool)(NRF_P0->IN & (1 << pin));
}

bool IR_NRF_GPIO_Read_Latch(uint8_t pin)
{
	bool latch = NRF_P0->LATCH & (1 << pin);
	NRF_P0->LATCH &= (1 << pin);
  return latch;
}

void IR_NRF_GPIO_PortEvent_Enable(void)
{
  NRF_GPIOTE->INTENSET = (1 << 31);
}

void IR_NRF_GPIO_PortEvent_Disable(void)
{
  NRF_GPIOTE->INTENCLR = (1 << 31);
}

//this function will overwrite all gpio input/output config
void IR_NRF_GPIO_EXTI_Pin_Init(uint8_t pin, IR_NRF_GPIO_ModeTypeDef mode, IR_NRF_GPIO_PlarityTypeDef polarity,
                               IR_NRF_GPIO_OutputTypeDef out_init, IR_NRF_GPIO_Interrupt_StreamTypeDef stream)
{
  NRF_GPIOTE->CONFIG[stream] = (out_init << 20) | (polarity << 16) | (pin << 8) | mode;
  NRF_GPIOTE->INTENSET = 1 << stream;
}

void IR_NRF_GPIO_EXTI_Pin_Deinit(IR_NRF_GPIO_Interrupt_StreamTypeDef stream)
{
  NRF_GPIOTE->INTENCLR = 1 << stream;
  NRF_GPIOTE->EVENTS_IN[stream] = 0;
  NRF_GPIOTE->CONFIG[stream] = 0x00;
}

__WEAK void IR_GPIOE_Callback(IR_NRF_GPIO_Interrupt_StreamTypeDef stream)
{
  IR_UNUSED_VAR(stream);
}
