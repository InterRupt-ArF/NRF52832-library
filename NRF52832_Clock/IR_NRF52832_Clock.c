#include "IR_NRF52832_Clock.h"

void IR_NRF_Clock_Init(bool ext_hf_clk_en,
                       IR_NRF_Clock_LFCLKSourceTypeDef lf_clk_src,
                       IR_NRF_Clock_LFCrystalTypeDef ext_lf_crystal,
                       IR_NRF_Clock_TracePortSpeedTypeDef trace_port_speed,
                       IR_NRF_Clock_TraceMUXTypeDef trace_mux)
{
  if(ext_hf_clk_en)
  {
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    while(!NRF_CLOCK->EVENTS_HFCLKSTARTED);
  }
  IR_NRF_Clock_LFCLK_Set_Source(lf_clk_src, ext_lf_crystal);
  IR_NRF_Clock_LFCLK_Start();
  IR_NRF_Clock_Set_TracePortSpeed(trace_port_speed);
  IR_NRF_Clock_Set_TraceMUX(trace_mux);
}

void IR_NRF_Clock_Deinit(void)
{
  //TODO:
}

void IR_NRF_Clock_Set_IRQ(IR_NRF_Clock_IRQTypeDef irq)
{
  NRF_CLOCK->INTENSET = irq;
}

void IR_NRF_Clock_Clear_IRQ(IR_NRF_Clock_IRQTypeDef irq)
{
  NRF_CLOCK->INTENCLR = irq;
}

//note: if u choose any src rather than "IR_NRF_CLOCK_LFCLKSOURCE_EXTERNAL", 
//the crystal parameter wont take any affect
void IR_NRF_Clock_LFCLK_Set_Source(IR_NRF_Clock_LFCLKSourceTypeDef src,
                                   IR_NRF_Clock_LFCrystalTypeDef crystal)
{
  if(src == IR_NRF_CLOCK_LFCLKSOURCE_EXTERNAL)
  {
    NRF_CLOCK->LFCLKSRC = (crystal << 16) | src;
  }
  else
  {
    NRF_CLOCK->LFCLKSRC = src;
  }
}

void IR_NRF_Clock_LFCLK_Start(void)
{
  NRF_CLOCK->TASKS_LFCLKSTART = 1;
  if((NRF_CLOCK->LFCLKSRC & 0x02) == IR_NRF_CLOCK_LFCLKSOURCE_EXTERNAL)
  {
    while(!NRF_CLOCK->EVENTS_LFCLKSTARTED);
  }
}


//1 <= ct <= 127
//time = (ct*0.25) S 
//0.25 <= time <= 31.75
//this register is retained
void IR_NRF_Clock_Set_CalibrationTimerInterval(uint8_t ct)
{
  CONSTRAIN(ct, CTIV_MAX, CTIV_MIN);
  NRF_CLOCK->CTIV = ct;
}

void IR_NRF_Clock_Set_TracePortSpeed(IR_NRF_Clock_TracePortSpeedTypeDef speed)
{
  NRF_CLOCK->TRACECONFIG = (NRF_CLOCK->TRACECONFIG & ~(0x03)) | speed;
}

void IR_NRF_Clock_Set_TraceMUX(IR_NRF_Clock_TraceMUXTypeDef mux)
{
  NRF_CLOCK->TRACECONFIG = (NRF_CLOCK->TRACECONFIG & ~(0x030000)) | mux;
}

IR_NRF_Clock_HFCLKSourceTypeDef IR_NRF_Clock_Get_HFCLK_Source(void)
{
  return(NRF_CLOCK->HFCLKSTAT & 0x01);
}

IR_NRF_Clock_HFCLKStateTypeDef IR_NRF_Clock_Get_HFCLK_State(void)
{
  return(NRF_CLOCK->HFCLKSTAT & 0x010000);
}

IR_NRF_Clock_LFCLKSourceTypeDef IR_NRF_Clock_Get_LFCLK_Source(void)
{
  return(NRF_CLOCK->HFCLKSTAT & 0x03);
}

IR_NRF_Clock_LFCLKStateTypeDef IR_NRF_Clock_Get_LFCLK_State(void)
{
  return(NRF_CLOCK->HFCLKSTAT & 0x010000);
}
