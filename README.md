# NRF52832-library
A simplified library for NRF52832

I am an amateur in embedded systems development and I had a lot of problem working around NRF52 SDK myself!!!
so I created this repo to write some simplified piece of code (in exchange for some overhead of course) for my future development.
I also hoped that this may shorten the learning time around NRF52832 for new users (newer than me :) ) of this device series.
