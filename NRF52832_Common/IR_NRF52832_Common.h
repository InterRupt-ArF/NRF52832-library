#ifndef __IR_NRF52832_COMMON_H__
#define __IR_NRF52832_COMMON_H__

#include "nrf52.h"
#include "stdint.h"
#include "stdbool.h"

#define CONSTRAIN(num, max, min) do{\
                                      if(num > max) num = max;\
                                      else if(num < min) num = min;\
                                 }while(0)
                                    
#define IR_UNUSED_VAR(var) ((void)(var))

typedef enum
{
	RESET = 0,
	SET   = 1
}FlagStatus;

#endif /*__IR_NRF52832_COMMON_H__*/
