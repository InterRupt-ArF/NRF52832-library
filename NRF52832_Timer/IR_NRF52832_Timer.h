/*
TODO:explanation
*/

#ifndef __IR_NRF52832_TIMER_H__
#define __IR_NRF52832_TIMER_H__

#include "IR_NRF52832_Common.h"

typedef enum
{
  IR_NRF_TIMER_TIMERMODE   = 0,
  IR_NRF_TIMER_COUNTERMODE = 2
}IR_NRF_Timer_ModeTypeDef;

typedef enum
{
  IR_NRF_TIMER_BITMODE_16 = 0,
  IR_NRF_TIMER_BITMODE_8  = 1,
  IR_NRF_TIMER_BITMODE_24 = 2,
  IR_NRF_TIMER_BITMODE_32 = 3
}IR_NRF_Timer_BitmodeTypeDef;

typedef enum
{
  IR_NRF_TIMER_PRESCALER_0 = 0,
  IR_NRF_TIMER_PRESCALER_1 = 1,
  IR_NRF_TIMER_PRESCALER_2 = 2,
  IR_NRF_TIMER_PRESCALER_3 = 3,
  IR_NRF_TIMER_PRESCALER_4 = 4,
  IR_NRF_TIMER_PRESCALER_5 = 5,
  IR_NRF_TIMER_PRESCALER_6 = 6,
  IR_NRF_TIMER_PRESCALER_7 = 7,
  IR_NRF_TIMER_PRESCALER_8 = 8,
  IR_NRF_TIMER_PRESCALER_9 = 9
}IR_NRF_Timer_PrescalerTypeDef;

typedef enum
{
  IR_NRF_TIMER_CHANNEL_0 = 0,
  IR_NRF_TIMER_CHANNEL_1 = 1,
  IR_NRF_TIMER_CHANNEL_2 = 2,
  IR_NRF_TIMER_CHANNEL_3 = 3,
  IR_NRF_TIMER_CHANNEL_4 = 4,
	IR_NRF_TIMER_CHANNEL_5 = 5
}IR_NRF_Timer_ChannelTypeDef;

void IR_NRF_Timer_Init(NRF_TIMER_Type* timer, IR_NRF_Timer_ModeTypeDef mode, IR_NRF_Timer_BitmodeTypeDef bitband,
                          IR_NRF_Timer_PrescalerTypeDef prescaler, bool irq_en, uint32_t irq_priority);
void IR_NRF_Timer_Deinit(NRF_TIMER_Type* timer);
void IR_NRF_Timer_Channel_Init(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel,
                              uint32_t capture_compare, bool event_en, bool repetetive_en);
void IR_NRF_Timer_Channel_Deinit(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel);
uint32_t IR_NRF_Timer_Get_Value(NRF_TIMER_Type* timer, IR_NRF_Timer_ChannelTypeDef channel);
void IR_NRF_Timer_Start(NRF_TIMER_Type* timer);
void IR_NRF_Timer_Stop(NRF_TIMER_Type* timer);
void IR_NRF_Timer_Reset(NRF_TIMER_Type* timer);
void IR_Timer0_Callback(IR_NRF_Timer_ChannelTypeDef channel);
void IR_Timer1_Callback(IR_NRF_Timer_ChannelTypeDef channel);
void IR_Timer2_Callback(IR_NRF_Timer_ChannelTypeDef channel);
void IR_Timer3_Callback(IR_NRF_Timer_ChannelTypeDef channel);
void IR_Timer4_Callback(IR_NRF_Timer_ChannelTypeDef channel);

#endif /*__IR_NRF52832_TIMER_H__*/
